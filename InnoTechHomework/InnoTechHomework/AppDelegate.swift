//
//  AppDelegate.swift
//  InnoTechHomework
//
//  Created by 陳維成 on 2021/3/20.
//  Copyright © 2021 WilsonChen. All rights reserved.
//

import UIKit
import SnapKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let initialViewController = storyboard.instantiateViewController(withIdentifier: "SearcbTableViewController")
        window?.rootViewController = initialViewController
        window?.makeKeyAndVisible()
        
        return true
    }



}

