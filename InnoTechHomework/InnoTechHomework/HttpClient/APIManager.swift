//
//  APIManager.swift
//  InnoTechHomework
//
//  Created by 陳維成 on 2021/3/20.
//  Copyright © 2021 WilsonChen. All rights reserved.
//

import Foundation
import Alamofire

protocol EndPointType {
    
    var baseURL: String { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var url: URL { get }
    var encoding: ParameterEncoding { get }
    
}

enum EndpointItem {
    case photos
}

extension EndpointItem: EndPointType {
    
    var baseURL: String {
        return "https://jsonplaceholder.typicode.com"
    }
    
    var path: String {
        switch self {
            
        case .photos:
            return "/photos"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .photos:
            return .get
        }
    }
    
    var url: URL {
        switch self {
        default:
            return URL(string: self.baseURL + self.path)!
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        default:
            return JSONEncoding.default
        }
    }
    
}

class APIManager {
    
    func fetchData<T>(type: EndPointType, params: Parameters? = nil, handler: @escaping ([T]?, _ error: Error?)->()) where T: Codable {
        
        request(type.url,
                method: type.httpMethod,
                parameters: params,
                encoding: type.encoding).validate().responseJSON { data in
                    switch data.result {
                    case .success(_):
                        let decoder = JSONDecoder()
                        if let jsonData = data.data {
                            let result = try! decoder.decode([T].self, from: jsonData)
                            handler(result,nil)
                        }
                        
                    case .failure(let error):
                        handler(nil,error)
                    }
        }
    }
    
}
