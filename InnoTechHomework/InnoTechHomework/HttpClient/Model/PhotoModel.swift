//
//  PhotoModel.swift
//  InnoTechHomework
//
//  Created by 陳維成 on 2021/3/20.
//  Copyright © 2021 WilsonChen. All rights reserved.
//

import Foundation

struct Photo: Codable {
    
    let albumId: Int
    let id: Int
    let title: String
    let url: String
    let thumbnailUrl: String
    
}
