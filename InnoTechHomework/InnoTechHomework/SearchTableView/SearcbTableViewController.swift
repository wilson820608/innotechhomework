//
//  ViewController.swift
//  InnoTechHomework
//
//  Created by 陳維成 on 2021/3/20.
//  Copyright © 2021 WilsonChen. All rights reserved.
//

import UIKit

class SearcbTableViewController: UIViewController {
    
   // MARK: Property
    var presenter = SearchTableVIewPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        layoutWithSearchBar()
        layoutWithTableView()
        setupDeleget()
        setupConstant()
        presenter.delegate = self
        presenter.getPhotos()
    }

    
    // MARK: UI
    private let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        return searchBar
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.estimatedRowHeight = 300
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(SearchTableViewCell.self, forCellReuseIdentifier: "SearchTableViewCell")
        
        return tableView
    }()
    
    
    private func layoutWithSearchBar() {
        view.addSubview(searchBar)
        searchBar.snp.makeConstraints{ maker in
            
            if #available(iOS 11.0, *) {
                 maker.top.equalTo(view.safeAreaLayoutGuide)
            } else {
                 maker.top.equalToSuperview()
            }
           
            maker.left.equalToSuperview()
            maker.right.equalToSuperview()
        }
    }
    
    private func layoutWithTableView() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints{ maker in
            maker.top.equalTo(searchBar.snp.bottom)
            maker.left.equalToSuperview()
            maker.right.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
    }
    
    private func setupDeleget() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func setupConstant() {
        searchBar.searchTextField.addTarget(self, action: #selector(fliterData), for: .editingChanged)
    }
    
    @objc private func fliterData() {
        guard let filterString = searchBar.searchTextField.text else { return }
        
        filterPhonerDatas = photoData.filter{ $0.title.contains(filterString) }
        tableView.reloadData()
        
    }
    
    private var filterPhonerDatas: [Photo] = []
    private var photoData: [Photo] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
}

extension SearcbTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       let model = (searchBar.searchTextField.text?.count ?? 0 > 0) ? filterPhonerDatas[indexPath.row] : photoData[indexPath.row]
        print("Photo(albumid: \(model.albumId), id: \(model.id), title: \(model.title), url: \(model.url), thumbnailUrl: \(model.thumbnailUrl)")
    }
}

extension SearcbTableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (searchBar.searchTextField.text?.count ?? 0 > 0) ? filterPhonerDatas.count : photoData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath) as! SearchTableViewCell

        let model = (searchBar.searchTextField.text?.count ?? 0 > 0) ? filterPhonerDatas[indexPath.row] : photoData[indexPath.row]
        cell.model = model
        return cell
    }
    
    
}

extension SearcbTableViewController: SearchTableVIewPresenterDelegate {
    func didSuccessResponse<T>(data: T) {
        
        if let data = data as? [Photo] {
            self.photoData = data
        }
        
    }
    
    func didFailResponse() {
        // TODO Fail Alert
    }
}

