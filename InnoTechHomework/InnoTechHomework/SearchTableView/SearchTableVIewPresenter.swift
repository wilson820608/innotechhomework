//
//  SearchTableVIewPresenter.swift
//  InnoTechHomework
//
//  Created by 陳維成 on 2021/3/21.
//  Copyright © 2021 WilsonChen. All rights reserved.
//

import UIKit

protocol SearchTableVIewPresenterDelegate {
    func didSuccessResponse<T>(data: T)
    func didFailResponse()
}

class SearchTableVIewPresenter {
    
    var delegate: SearchTableVIewPresenterDelegate?

    func getPhotos() {
        
        APIManager().fetchData(type: EndpointItem.photos) { (response: [Photo]?, error) in
            guard let response = response else {
                self.delegate?.didFailResponse()
                return
            }
            self.delegate?.didSuccessResponse(data: response)
        }
    }
}


