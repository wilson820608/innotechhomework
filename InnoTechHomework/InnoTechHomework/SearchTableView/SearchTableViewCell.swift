//
//  SearchTableViewCell.swift
//  InnoTechHomework
//
//  Created by 陳維成 on 2021/3/20.
//  Copyright © 2021 WilsonChen. All rights reserved.
//

import UIKit
import SDWebImage

class SearchTableViewCell: UITableViewCell {


    var model: Photo? {
        didSet {
            makeRow()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    
    let photoImageView = UIImageView()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        
        return label
    }()
    
    private func setup() {
        layoutWithPhotoImageView()
        layoutWithNameLabel()
    }
    
    private func layoutWithPhotoImageView() {
        contentView.addSubview(photoImageView)
        photoImageView.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(contentView)
            maker.top.equalToSuperview()
        }
    }
    
    private func layoutWithNameLabel() {
        contentView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(photoImageView.snp.bottom)
            maker.left.right.bottom.equalTo(contentView).offset(0)
        }
    }
    
    private func makeRow() {
        
        
        guard let model = model else {
            return
        }
        
        setup()
        nameLabel.text = model.title
        photoImageView.sd_setImage(with: URL(string: model.url)) {[weak self] (image, error, _, _) in
            guard let self = self,
                  let tableView =  self.superview as? UITableView,
                  let indexpath = tableView.indexPath(for: self)
            else { return }
            
            tableView.reloadRows(at: [indexpath], with: .automatic)
        }
    }

}
